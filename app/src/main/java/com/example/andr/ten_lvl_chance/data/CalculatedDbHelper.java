package com.example.andr.ten_lvl_chance.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.andr.ten_lvl_chance.data.DataContract.DatabaseEntry;

public class CalculatedDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = DatabaseEntry.DATABASE_NAME;
    private static final int DATABASE_VERSION = Integer.parseInt(DatabaseEntry.DATABASE_VERSION);


    public CalculatedDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_CALCULATES_TABLE = "CREATE TABLE IF NOT EXISTS " + DatabaseEntry.TABLE_NAME + " ("
                + DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseEntry.LVL_1_QUANTITIES + " INTEGER, "//MainActivity.int [] mMiddleLevelsTrys
                + DatabaseEntry.LVL_2_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_3_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_4_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_5_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_6_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_7_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_8_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_9_QUANTITIES + " INTEGER, "
                + DatabaseEntry.LVL_1_COST + " INTEGER, "//MainActivity.int [] mMiddleLevelsSummaryPrice;
                + DatabaseEntry.LVL_2_COST + " INTEGER, "
                + DatabaseEntry.LVL_3_COST + " INTEGER, "
                + DatabaseEntry.LVL_4_COST + " INTEGER, "
                + DatabaseEntry.LVL_5_COST + " INTEGER, "
                + DatabaseEntry.LVL_6_COST + " INTEGER, "
                + DatabaseEntry.LVL_7_COST + " INTEGER, "
                + DatabaseEntry.LVL_8_COST + " INTEGER, "
                + DatabaseEntry.LVL_9_COST + " INTEGER, "
                + DatabaseEntry.CYCLES_EXECUTED + " INTEGER, " //MainActivity.mCyclesExecutionCounter
                + DatabaseEntry.MIDDLE_COSTS_NUMBER + " INTEGER, " // LONG!!! MainActivity.mMiddleAllSummaryPrice
                + DatabaseEntry.MIDDLE_QUANTITIES_NUMBER + " INTEGER);";//MainActivity.mMiddleCounter

        sqLiteDatabase.execSQL(SQL_CREATE_CALCULATES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
