package com.example.andr.ten_lvl_chance;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

public class GraphCountActivity extends AppCompatActivity {

    private long [] mCyclesCounters;
    private DataPoint [] dataPointsArray;
    private static final int MIN_GRAPH_Y_VALUE = 0;
    private static final int MIN_GRAPH_X_VALUE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_count);
        mCyclesCounters = MainActivity.getInstance().getCountersForCycle();
        dataPointsArray = new DataPoint[mCyclesCounters.length];
        convertArrayToSeries();
        String titleString = getResources().getString(R.string.graph_middle_counters_legend);


        GraphView graph = (GraphView) findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(dataPointsArray);
        /*PointsGraphSeries<DataPoint> series = new PointsGraphSeries<DataPoint>(new DataPoint[] {
                //for (int i =0; i <= mCyclesCounters.length; i++) {new DataPoint(i, i)}
                new DataPoint(testArray[1], testArray[2]),

                new DataPoint(0, 1),
                new DataPoint(1, 5),
                new DataPoint(2, 3),
                new DataPoint(3, 2),
                new DataPoint(4, 6)
        });*/

        // set manual X bounds
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(MIN_GRAPH_Y_VALUE);
        graph.getViewport().setMaxY(maxArrayElementValue());

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(MIN_GRAPH_X_VALUE);
        graph.getViewport().setMaxX(mCyclesCounters.length);

        // enable scaling and scrolling
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);

        // legend
        series.setTitle(titleString);
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        graph.addSeries(series);
        /*series.setShape(PointsGraphSeries.Shape.POINT);
        series.setColor(Color.RED);*/
    }

    private void convertArrayToSeries() {
        for (int i = 0; i < mCyclesCounters.length; i++){
            dataPointsArray[i] = new DataPoint(i, mCyclesCounters[i]);
        }
    }

    private long maxArrayElementValue () {
        long maxValue = 0;
        for (int i = 0; i < mCyclesCounters.length; i++) {
            if (mCyclesCounters[i] > maxValue) {
                maxValue = mCyclesCounters[i];
            }
        }
        return maxValue;
    }
}
