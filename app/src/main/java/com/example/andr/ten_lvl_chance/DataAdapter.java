package com.example.andr.ten_lvl_chance;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {
    private int [] mLevels;
    private int [] mLevelsTrys;
    private int [] mLevelsSummaryPrice;

    DataAdapter(int[] mLevelsInc, int[] mLevelsTrysInc, int[] mLevelsSummaryPriceInc) {
        mLevels = mLevelsInc;
        mLevelsTrys = mLevelsTrysInc;
        mLevelsSummaryPrice = mLevelsSummaryPriceInc;

    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView mTextView;
        TextView tvLvlsTrys;
        TextView tvLvlSummPrice;
        public MyViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.levelText); //v
            tvLvlsTrys = (TextView) v.findViewById(R.id.quantity_of_attempts);
            tvLvlSummPrice = (TextView) v.findViewById(R.id.summary_price);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.three_text_columns_lin_lay, viewGroup, false);
        MyViewHolder viewHolder = new MyViewHolder (mView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.mTextView.setText(String.valueOf(mLevels[i]));
        myViewHolder.tvLvlsTrys.setText(String.valueOf(mLevelsTrys[i]));
        myViewHolder.tvLvlSummPrice.setText(String.valueOf(mLevelsSummaryPrice[i]));

    }

    @Override
    public int getItemCount() {
        return mLevels.length;
    }


}
