package com.example.andr.ten_lvl_chance;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.andr.ten_lvl_chance.data.CalculatedDbHelper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static MainActivity instance;
    public RecyclerView mRecyclerView;
    public RecyclerView.Adapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;
    private int[] mLevelsTrys;
    private int[] mLevelsSummaryPrice;
    private int[] mLevels;
    private int[] mMiddleLevelsTrys;
    private int[] mMiddleLevelsSummaryPrice;
    private static int mCyclesExecution = 10; //TODO Return 1000
    private int mCyclesExecutionCounter = 0;
    private int mMaxLevel = 10;
    private final int PRICE = 100;
    private int mCounter;
    private int mSummaryPrice;
    private int mMiddleCounter;
    private long mMiddleAllSummaryPrice = 0L;
    private ProgressDialog mDialog;
    private boolean isCalculating;
    private Button mStartBtn;
    private Button mWorkWithDataBtn;
    private TextView tvMiddleValue;
    private long[] mCountersForCycle;
    private long[] mSummaryPriceForCycle;
    private ArrayList DataToBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;
        tvMiddleValue = findViewById(R.id.tvMiddleValue);
        tvMiddleValue.setText(getResources().getString(R.string.middle_value));
        mLevels = new int[mMaxLevel];
        for (int i = 0; i < mLevels.length; i++) {
            mLevels[i] = i;
        }
        mStartBtn = findViewById(R.id.btn_start);
        mWorkWithDataBtn = findViewById(R.id.btn_workWithData);
        mWorkWithDataBtn.setEnabled(false);
    }

    public void onBtnStartClick(View view) {
        CalcAsyncTask calcAsyncTask = new CalcAsyncTask();
        calcAsyncTask.execute();
    }

    public void onBtnWorkWithDataClick(View view) {
        setDataToBase(); //TODO Think, where array collect will right to collect.
        Intent intent = new Intent(MainActivity.this, DataMenuActivity.class);
        startActivity(intent);
    }

    protected void addForGraphCalculating() {
        mCountersForCycle[mCyclesExecutionCounter] = mCounter;
        mSummaryPriceForCycle[mCyclesExecutionCounter] = mSummaryPrice;
    }

    protected long[] getCountersForCycle() {
        return mCountersForCycle;
    }

    private void calculateUpgrade() {
        int lvl = 1;
        int price = PRICE;
        mSummaryPrice = 0;
        mCounter = 0;
        double chance_fell_out = 0.0d;
        double chance_needed = 0.9d;
        mLevelsTrys = new int[10];
        mLevelsSummaryPrice = new int[mMaxLevel + 1];
        while (lvl <= 9) {
            chance_fell_out = Math.random();
            mLevelsTrys[lvl]++;
            if (chance_fell_out < chance_needed) {
                lvl++;
                price = price + 100;
                chance_needed = chance_needed - 0.1;

            } else {
                lvl = 1;
                price = PRICE;
                chance_needed = 0.9;
            }
            mLevelsSummaryPrice[lvl] = mLevelsSummaryPrice[lvl] + price;
            mSummaryPrice = mSummaryPrice + price;
            mCounter++;
        }
        mCyclesExecutionCounter++;
    }

    private void addForMiddleCalculating() {
        for (int i = 0; i < mMaxLevel; i++) {
            mMiddleLevelsTrys[i] = mMiddleLevelsTrys[i] + mLevelsTrys[i];
            mMiddleLevelsSummaryPrice[i] = mMiddleLevelsSummaryPrice[i] + mLevelsSummaryPrice[i];
        }
        mMiddleCounter = mMiddleCounter + mCounter;
        mMiddleAllSummaryPrice = mMiddleAllSummaryPrice + mSummaryPrice;
    }

    public void startProgressBarDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getResources().getString(R.string.execution_progress));
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setProgress(0);
        mDialog.setMax(mCyclesExecution);
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public void callOfRecycleView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_lvls);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new DataAdapter(arrayFirstElementCutter(mLevels), arrayFirstElementCutter(mMiddleLevelsTrys), arrayFirstElementCutter(mMiddleLevelsSummaryPrice)); //for single mLevelsTrys, mLevelsSummaryPrice
        mRecyclerView.setAdapter(mAdapter);
    }

    private int[] arrayFirstElementCutter(int[] someIncomeArray) {
        int[] cuttedArray = new int[someIncomeArray.length - 1];
        for (int i = 1; i <= cuttedArray.length; i++) {
            cuttedArray[i - 1] = someIncomeArray[i];
        }
        return cuttedArray;
    }

    private long[] arrayFirstElementCutter(long[] someIncomeArray) {
        long[] cuttedArray = new long[someIncomeArray.length - 1];
        for (int i = 1; i <= cuttedArray.length; i++) {
            cuttedArray[i - 1] = someIncomeArray[i];
        }
        return cuttedArray;
    }

    private class CalcAsyncTask extends AsyncTask<Void, Integer, Void> {


        @Override
        protected void onPreExecute() {
            isCalculating = true;
            mStartBtn.setEnabled(false);
            mWorkWithDataBtn.setEnabled(false);
            mCyclesExecutionCounter = 0;
            mMiddleLevelsTrys = new int[mMaxLevel];
            mMiddleLevelsSummaryPrice = new int[mMaxLevel + 1];
            mCountersForCycle = new long[mCyclesExecution + 1];
            mSummaryPriceForCycle = new long[mCyclesExecution + 1];
            startProgressBarDialog();
            mDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            mDialog.setProgress(values[0]);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < mCyclesExecution; i++) {
                calculateUpgrade();
                addForMiddleCalculating();
                addForGraphCalculating();
                publishProgress(mCyclesExecutionCounter);
            }
            for (int i = 0; i < mMaxLevel; i++) {
                mMiddleLevelsTrys[i] = mMiddleLevelsTrys[i] / mCyclesExecution;
                mMiddleLevelsSummaryPrice[i] = mMiddleLevelsSummaryPrice[i] / mCyclesExecution;
            }
            mMiddleAllSummaryPrice = mMiddleAllSummaryPrice / (long) mCyclesExecution;

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            isCalculating = false;
            arrayFirstElementCutter(mCountersForCycle);
            arrayFirstElementCutter(mSummaryPriceForCycle);
            mDialog.dismiss();
            mStartBtn.setEnabled(true);
            mWorkWithDataBtn.setEnabled(true);
            tvMiddleValue.setText(textForMiddleValueTextView());
            callOfRecycleView();
            TextView tv_counter = findViewById(R.id.tv_counter);
            tv_counter.setText(textForSummaryTextView());
        }
    }

    private String textForMiddleValueTextView() {
        String firstPart = getResources().getString(R.string.txt_middle_value_part_one);
        String secondPart = getResources().getString(R.string.txt_middle_value_part_two);
        return firstPart + mCyclesExecution + secondPart;
    }

    private String textForSummaryTextView() {
        String all_quantities = getResources().getString(R.string.all_quantities);
        String all_consumptions_summary = getResources().getString(R.string.all_consumptions_summary);
        String full_cycles_number = getResources().getString(R.string.full_cycles_number);
        String middle_price_for_all_cycles_part_one = getResources().getString(R.string.middle_price_for_all_cycles_part_one);
        String middle_price_for_all_cycles_part_two = getResources().getString(R.string.middle_price_for_all_cycles_part_two);
        return all_quantities + mCounter + "\n" + all_consumptions_summary + mSummaryPrice + "\n" + full_cycles_number + mCyclesExecutionCounter + "\n" + middle_price_for_all_cycles_part_one + mCyclesExecution + middle_price_for_all_cycles_part_two + mMiddleAllSummaryPrice;
    }

    void setDataToBase() {
        DataToBase = new ArrayList();
        for (int i : mMiddleLevelsTrys
                ) {
            DataToBase.add(i);

        }
        for (int i : mMiddleLevelsSummaryPrice
                ) {
            DataToBase.add(i);
        }
        DataToBase.add(mCyclesExecutionCounter);
        DataToBase.add(mMiddleCounter); //Placed instead Long from mAllSummaryPrice
        DataToBase.add(mMiddleCounter);

    }

    public ArrayList getDataToBase() {
        return DataToBase;
    }

}
