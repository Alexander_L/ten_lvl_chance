package com.example.andr.ten_lvl_chance.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.andr.ten_lvl_chance.MainActivity;

import java.util.ArrayList;

import static com.example.andr.ten_lvl_chance.data.DataContract.DatabaseEntry.DATABASE_NAME;
import static com.example.andr.ten_lvl_chance.data.DataContract.DatabaseEntry.TABLE_NAME;

public class WorkWithDatabase {
    private CalculatedDbHelper calculatedDbHelper;
    private SQLiteDatabase sqLiteDatabase;

    public WorkWithDatabase(Context context) {
        calculatedDbHelper = new CalculatedDbHelper(context);
        sqLiteDatabase = calculatedDbHelper.getWritableDatabase();
    }


    class WriteToDatabaseTask extends AsyncTask<Void, Void, Void> {

        ContentValues contentValues = new ContentValues();

        @Override
        protected Void doInBackground(Void... voids) {

            ArrayList DataToSave = new ArrayList();
            DataToSave = MainActivity.getInstance().getDataToBase();
            contentValues.put(DataContract.DatabaseEntry.LVL_1_QUANTITIES, (Integer) DataToSave.get(0));
            contentValues.put(DataContract.DatabaseEntry.LVL_2_QUANTITIES, (Integer) DataToSave.get(1));
            contentValues.put(DataContract.DatabaseEntry.LVL_3_QUANTITIES, (Integer) DataToSave.get(2));
            contentValues.put(DataContract.DatabaseEntry.LVL_4_QUANTITIES, (Integer) DataToSave.get(3));
            contentValues.put(DataContract.DatabaseEntry.LVL_5_QUANTITIES, (Integer) DataToSave.get(4));
            contentValues.put(DataContract.DatabaseEntry.LVL_6_QUANTITIES, (Integer) DataToSave.get(5));
            contentValues.put(DataContract.DatabaseEntry.LVL_7_QUANTITIES, (Integer) DataToSave.get(6));
            contentValues.put(DataContract.DatabaseEntry.LVL_8_QUANTITIES, (Integer) DataToSave.get(7));
            contentValues.put(DataContract.DatabaseEntry.LVL_9_QUANTITIES, (Integer) DataToSave.get(8));
            contentValues.put(DataContract.DatabaseEntry.LVL_1_COST, (Integer) DataToSave.get(9));
            contentValues.put(DataContract.DatabaseEntry.LVL_2_COST, (Integer) DataToSave.get(10));
            contentValues.put(DataContract.DatabaseEntry.LVL_3_COST, (Integer) DataToSave.get(11));
            contentValues.put(DataContract.DatabaseEntry.LVL_4_COST, (Integer) DataToSave.get(12));
            contentValues.put(DataContract.DatabaseEntry.LVL_5_COST, (Integer) DataToSave.get(13));
            contentValues.put(DataContract.DatabaseEntry.LVL_6_COST, (Integer) DataToSave.get(14));
            contentValues.put(DataContract.DatabaseEntry.LVL_7_COST, (Integer) DataToSave.get(15));
            contentValues.put(DataContract.DatabaseEntry.LVL_8_COST, (Integer) DataToSave.get(16));
            contentValues.put(DataContract.DatabaseEntry.LVL_9_COST, (Integer) DataToSave.get(17));
            contentValues.put(DataContract.DatabaseEntry.CYCLES_EXECUTED, (Integer) DataToSave.get(18));
            contentValues.put(DataContract.DatabaseEntry.MIDDLE_COSTS_NUMBER, (Integer) DataToSave.get(19)); //LONG here!
            contentValues.put(DataContract.DatabaseEntry.MIDDLE_QUANTITIES_NUMBER, (Integer) DataToSave.get(20));

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            sqLiteDatabase = calculatedDbHelper.getWritableDatabase();
            sqLiteDatabase.insert(DataContract.DatabaseEntry.TABLE_NAME, null, contentValues);
        }
    }

    public void dropTable() {
        sqLiteDatabase.delete(TABLE_NAME, null, null);
        //sqLiteDatabase.close();
    } //not secured?

    public void saveDataToDatabase() {
        WriteToDatabaseTask writeToDatabaseTask = new WriteToDatabaseTask();
        writeToDatabaseTask.execute();
    }

    public boolean testMetForBase() {
        boolean result = false;
        if (sqLiteDatabase != null) {
            result = true;
        }
        return result;
    }

    public int getAutoincrementID() {
        SQLiteDatabase db = calculatedDbHelper.getReadableDatabase();
        String[] columnToTest = new String[1];
        int rowIdElementsSum = 0;
        columnToTest[0] = DataContract.DatabaseEntry._ID;
        Cursor cursor = db.query(DataContract.DatabaseEntry.TABLE_NAME, columnToTest, null, null, null, null, null);
        while (cursor.moveToNext()) {
            rowIdElementsSum++;
        }
        return rowIdElementsSum;
    }

    private void reserveCreateTable(){
        String SQL_CREATE_CALCULATES_TABLE = "CREATE TABLE IF NOT EXISTS " + DataContract.DatabaseEntry.TABLE_NAME + " ("
                + DataContract.DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DataContract.DatabaseEntry.LVL_1_QUANTITIES + " INTEGER, "//MainActivity.int [] mMiddleLevelsTrys
                + DataContract.DatabaseEntry.LVL_2_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_3_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_4_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_5_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_6_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_7_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_8_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_9_QUANTITIES + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_1_COST + " INTEGER, "//MainActivity.int [] mMiddleLevelsSummaryPrice;
                + DataContract.DatabaseEntry.LVL_2_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_3_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_4_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_5_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_6_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_7_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_8_COST + " INTEGER, "
                + DataContract.DatabaseEntry.LVL_9_COST + " INTEGER, "
                + DataContract.DatabaseEntry.CYCLES_EXECUTED + " INTEGER, " //MainActivity.mCyclesExecutionCounter
                + DataContract.DatabaseEntry.MIDDLE_COSTS_NUMBER + " INTEGER, " // LONG!!! MainActivity.mMiddleAllSummaryPrice
                + DataContract.DatabaseEntry.MIDDLE_QUANTITIES_NUMBER + " INTEGER);";//MainActivity.mMiddleCounter

        sqLiteDatabase.execSQL(SQL_CREATE_CALCULATES_TABLE);
    }

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }
}
