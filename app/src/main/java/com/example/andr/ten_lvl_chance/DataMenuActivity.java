package com.example.andr.ten_lvl_chance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.andr.ten_lvl_chance.data.WorkWithDatabase;

public class DataMenuActivity extends AppCompatActivity {
    private WorkWithDatabase workWithDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_menu);
        workWithDatabase = new WorkWithDatabase(this); //TODO Remove this, every "back button" recreate this class.
    }

    public void onBtnGraphForCurrCompClick(View view) {
        Intent intent = new Intent(DataMenuActivity.this, GraphCountActivity.class);
        startActivity(intent);
    }

    public void onBtnGraphForAllDataFromBaseClick(View view) {
        String ver = String.valueOf(workWithDatabase.testMetForBase());
        Toast.makeText(this, ver, Toast.LENGTH_SHORT).show();
    }

    public void onBtnAllDataToDatabaseClick(View view) {
        workWithDatabase.saveDataToDatabase();
        int testIdsNumber = workWithDatabase.getAutoincrementID();
        testIdsNumber++; //This for correct Toast for user, first record dispaly not zero, as in base, but one.
        Toast.makeText(this, "Strok v base " + String.valueOf(testIdsNumber), Toast.LENGTH_SHORT).show();
    }

    public void onBtnClearDatabaseClick(View view) {
        workWithDatabase.dropTable();
        int testIdsNumber = workWithDatabase.getAutoincrementID();
        Toast.makeText(this, "Strok v base " + String.valueOf(testIdsNumber), Toast.LENGTH_SHORT).show();
    }

}
